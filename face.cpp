/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - face.cpp
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <string.h>
#include <string>

using namespace std;
using namespace cv;

float deg2rad = M_PI / 180;

/** Global variables */
String cascade_name = "dartcascade/cascade.xml";
CascadeClassifier cascade;

/** @function detectAndDisplay */
void detectAndDisplay( Mat frame )
{
	std::vector<Rect> faces;
	Mat frame_gray;

	// 1. Prepare Image by turning it into Grayscale and normalising lighting
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );

	// 2. Perform Viola-Jones Object Detection
	cascade.detectMultiScale( frame_gray, faces, 1.1, 1, 0|CV_HAAR_SCALE_IMAGE, Size(50, 50), Size(500,500) );

       // 3. Print number of Faces found
	std::cout << faces.size() << std::endl;

       // 4. Draw box around faces found
	for( int i = 0; i < faces.size(); i++ )
	{
		rectangle(frame, Point(faces[i].x, faces[i].y), Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height), Scalar( 0, 255, 0 ), 2);
	}

}

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

void our_sobel(Mat &input, Mat &mag, Mat &grad) {
	Mat x_kern, y_kern, output_x, output_y;
	int ddepth = -1;
	int kernel_size = 3;

	//Create x and y kernels to differentiate image
	x_kern = Mat::zeros(kernel_size, kernel_size, CV_32F);
	y_kern = Mat::zeros(kernel_size, kernel_size, CV_32F);

	x_kern.at<float>(0,0) = 1;
	x_kern.at<float>(0,2) = -1;
	x_kern.at<float>(1,0) = 2;
	x_kern.at<float>(1,2) = -2;
	x_kern.at<float>(2,0) = 1;
	x_kern.at<float>(2,2) = -1;

	y_kern.at<float>(0,0) = 1;
	y_kern.at<float>(0,1) = 2;
	y_kern.at<float>(0,2) = 1;
	y_kern.at<float>(2,0) = -1;
	y_kern.at<float>(2,1) = -2;
	y_kern.at<float>(2,2) = -1;

	//Apply kernels
	filter2D(input, output_x, CV_32F, x_kern);
	filter2D(input, output_y, CV_32F, y_kern);

	//Combine diffed images to make mag
	magnitude(output_x, output_y, mag);

	//Combine diffed images to make grad (little bit harder)
	grad = Mat(mag.rows, mag.cols, CV_32F);

	for (int i=0; i<output_x.rows; i++) {
		for (int j=0; j<output_x.cols; j++) {
			grad.at<float>(i,j) = atan(output_y.at<float>(i,j) / output_x.at<float>(i,j));
		}
	}

	//Free matricies
	x_kern.release();
	y_kern.release();
	output_x.release();
	output_y.release();
}

int getYfromX(int x, int rho, int theta) {
	return (int)round((rho - (x * cos(theta * deg2rad))) / sin(theta * deg2rad));
}

int getXfromY(int y, int rho, int theta) {
	return (int)round((rho - (y * sin(theta * deg2rad))) / cos(theta * deg2rad));
}

//TODO Theta=0 cases
void plot_hough(Mat &output, int rho, int theta) {
	int y, x;
	Point p1, p2;
	int a = 0;

	if (theta == 0) {
		int y = rho / cos(deg2rad * theta);
		for (int x=0; x<output.cols; x++) {
			//printf ("0 THETA - %d %d", x, y);
			output.at<Vec3f>(y,x)[0] = 1.0;
		}
	}
	else {
		x = 0;
		y = getYfromX(x, rho, theta);
		if (y >= 0 && y < output.cols) {
			p1 = Point(y, x);
			a = 1;
		}
		x = output.cols - 1;
		y = getYfromX(x, rho, theta);
		if (y >= 0 && y < output.cols) {
			if (a == 0) {
				p1 = Point(y, x);
			}
			else {
				p2 = Point(y, x);
			}
		}

		y = 0;
		x = getXfromY(y, rho, theta);
		if (x >= 0 && x < output.rows) {
			if (a == 0) {
				p1 = Point(y, x);
			}
			else {
				p2 = Point(y, x);
			}
		}

		y = output.rows - 1;
		x = getXfromY(y, rho, theta);
		if (x >= 0 && x < output.rows) {
			p2 = Point(x, y);
		}
	}


	//Plot lines
	line(output, p1, p2, Scalar(0, 0, 1), 1, 8);

	/*
	if (theta == 0) {
		int x = rho / cos(theta);
		for (int y=0; y<output.cols; y++) {
			//printf ("0 THETA - %d %d", x, y);
			output.at<Vec3f>(x,y)[0] = 1.0;
		}
	}
	/*else if (theta == 90) {
		int y = rho / sin(theta);
		for (int x=0; x<output.rows; x++) {
			//printf ("0 THETA - %d %d", x, y);
			output.at<Vec3f>(x,y)[0] = 1.0;
		}
	}
	else {
		for (int x=0; x<output.rows; x++) {
			y = (int)round((rho - (x * cos(theta * deg2rad))) / sin(theta * deg2rad));
			//printf ("DEBUG[%d %d] %d %d = %d %d\n", output.rows, output.cols, rho, theta, x, y);
			if (y >= 0 && y<output.cols) {
				//printf ("GOT THROUGH! - %d %d\n\n", x, y);
				output.at<Vec3f>(x,y)[2] = 1.0;
			}
		}
	}
	*/
}

//TODO Graph still does a weird overlap thing, maybe use vertical profile?
void our_hough(Mat &input, Mat &grad, Mat &acc, Mat &output) {
	//Declare
	float acc_width = sqrt(pow(input.rows, 2) + pow(input.cols, 2));
	//printf ("DEBUG: %f\n", acc_width);
	acc = Mat(180, (int)round(acc_width), CV_32F);
	//output = Mat::zeros(input.rows, input.cols, CV_32F);
	cvtColor(input, output, CV_GRAY2BGR);
	float rho;
	float highest = 0;
	float x, y;

	//Accumulate
	//TODO Change to use grad information
	for (int i=0; i<input.rows; i++) {
		for (int j=0; j<input.cols; j++) {
			if (input.at<float>(i,j) == 1.0) {
				for (int theta=0; theta<180; theta++) {
					//rho = sqrt(pow(i, 2) + pow(j, 2));
					rho = (i * cos(deg2rad * theta)) + (j * sin(deg2rad * theta));
					acc.at<float>(theta, (int)round(rho)) += 0.0025;
					if (acc.at<float>(theta, (int)round(rho)) > highest) {
						highest = acc.at<float>(theta, (int)round(rho));
					}
					//printf ("[%f] %d %d %d %d = %f\n", highest, i, j, theta, (int)round(rho), acc.at<float>(theta, round(rho)));
				}
			}
			//printf ("a\n");
		}
	}

	printf("\n\nLovely jubbly");
	getchar();
	//Threshold
	for (int theta=0; theta<acc.rows; theta++) {
		for (int rho=0; rho<acc.cols; rho++) {
			if (acc.at<float>(theta,rho) >= highest * 0.7) {
				printf ("%d %d %f %f %f\n", theta, rho, acc.at<float>(theta,rho), x, y);
				plot_hough(output, rho, theta);
				//Output selected line
			}
		}
	}

}

//TODO Make refinment function for Viola-Jones based off liney boiz

/** @function main */
int main( int argc, const char** argv )
{
	//Check correct # of args
	if (argc != 3) {
		printf("--(!)Incorrect # of args = %d\n", argc);
		return -1;
	}

  //Read Input Image
	Mat frame = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
	Mat input;
	frame.convertTo(input, CV_32F, 1/255.0);
	frame.release();

	//Concat strings so to number output files
	String file_name = "output/detected";
	file_name = file_name + argv[2] + ".jpg";
	std::cout << "OUTPUT: " << file_name << "\n";

	//Load the Strong Classifier in a structure called `Cascade'
	if( !cascade.load( cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };

	//Detect Faces and Display Result
	//detectAndDisplay( frame );

	//Delare various matricies
	Mat mag, grad, thresh, acc, output;

	//Blur image for better edge detection
	Size s = Size(5, 5);
	GaussianBlur(input, input, s, 5);

	our_sobel(input, mag, grad);

	//Threshold
	thresh = Mat(mag.rows, mag.cols, CV_32F);
	float t = 0.5;
	for (int i=0; i<mag.rows; i++) {
		for (int j=0; j<mag.cols; j++) {
			if (mag.at<float>(i,j) > t) {
				thresh.at<float>(i,j) = 1.0;
			}
			else {
				thresh.at<float>(i,j) = 0;
			}
		}
	}



	//Convert into Hough space
	printf ("RUNNING HOUGH\n");
	our_hough(thresh, grad, acc, output);
	printf ("BACK IN MAIN\n");

	//Save Result Image
	//imwrite( file_name, frame );
	namedWindow("Display window", CV_WINDOW_AUTOSIZE);
	imshow("Source", input);
	waitKey(0);
	imshow("Output Mag", mag);
	waitKey(0);
	imshow("Output Grad", grad);
	waitKey(0);
	imshow("Output Mag Threshhold", thresh);
	waitKey(0);
	imshow("Output Hough", output);
	waitKey(0);
	imshow("Output Hough Graph", acc);
	waitKey(0);

	//Free matricies
	input.release();
	mag.release();
	grad.release();
	thresh.release();
	output.release();
	acc.release();

	return 0;
}
